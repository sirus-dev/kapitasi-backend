'use strict';

/**
 * Refkecamatan.js controller
 *
 * @description: A set of functions called "actions" for managing `Refkecamatan`.
 */

module.exports = {

  /**
   * Retrieve refkecamatan records.
   *
   * @return {Object|Array}
   */

  find: async (ctx) => {
    if (ctx.query._q) {
      return strapi.services.refkecamatan.search(ctx.query);
    } else {
      return strapi.services.refkecamatan.fetchAll(ctx.query);
    }
  },

  /**
   * Retrieve a refkecamatan record.
   *
   * @return {Object}
   */

  findOne: async (ctx) => {
    if (!ctx.params._id.match(/^[0-9a-fA-F]{24}$/)) {
      return ctx.notFound();
    }

    return strapi.services.refkecamatan.fetch(ctx.params);
  },

  /**
   * Count refkecamatan records.
   *
   * @return {Number}
   */

  count: async (ctx) => {
    return strapi.services.refkecamatan.count(ctx.query);
  },

  /**
   * Create a/an refkecamatan record.
   *
   * @return {Object}
   */

  create: async (ctx) => {
    return strapi.services.refkecamatan.add(ctx.request.body);
  },

  /**
   * Update a/an refkecamatan record.
   *
   * @return {Object}
   */

  update: async (ctx, next) => {
    return strapi.services.refkecamatan.edit(ctx.params, ctx.request.body) ;
  },

  /**
   * Destroy a/an refkecamatan record.
   *
   * @return {Object}
   */

  destroy: async (ctx, next) => {
    return strapi.services.refkecamatan.remove(ctx.params);
  }
};
