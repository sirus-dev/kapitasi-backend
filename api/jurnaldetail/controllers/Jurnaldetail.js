'use strict';

/**
 * Jurnaldetail.js controller
 *
 * @description: A set of functions called "actions" for managing `Jurnaldetail`.
 */

module.exports = {

  /**
   * Retrieve jurnaldetail records.
   *
   * @return {Object|Array}
   */

  find: async (ctx) => {
    if (ctx.query._q) {
      return strapi.services.jurnaldetail.search(ctx.query);
    } else {
      return strapi.services.jurnaldetail.fetchAll(ctx.query);
    }
  },

  /**
   * Retrieve a jurnaldetail record.
   *
   * @return {Object}
   */

  findOne: async (ctx) => {
    if (!ctx.params._id.match(/^[0-9a-fA-F]{24}$/)) {
      return ctx.notFound();
    }

    return strapi.services.jurnaldetail.fetch(ctx.params);
  },

  /**
   * Count jurnaldetail records.
   *
   * @return {Number}
   */

  count: async (ctx) => {
    return strapi.services.jurnaldetail.count(ctx.query);
  },

  /**
   * Create a/an jurnaldetail record.
   *
   * @return {Object}
   */

  create: async (ctx) => {
    return strapi.services.jurnaldetail.add(ctx.request.body);
  },

  /**
   * Update a/an jurnaldetail record.
   *
   * @return {Object}
   */

  update: async (ctx, next) => {
    return strapi.services.jurnaldetail.edit(ctx.params, ctx.request.body) ;
  },

  /**
   * Destroy a/an jurnaldetail record.
   *
   * @return {Object}
   */

  destroy: async (ctx, next) => {
    return strapi.services.jurnaldetail.remove(ctx.params);
  }
};
