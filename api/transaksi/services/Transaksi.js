'use strict';

const moment = require('moment');

/**
 * Transaksi.js service
 *
 * @description: A set of functions similar to controller's actions to avoid code duplication.
 */

// Public dependencies.
const _ = require('lodash');

module.exports = {

  /**
   * Promise to fetch all transaksis.
   *
   * @return {Promise}
   */

  fetchAll: (params) => {
    // Convert `params` object to filters compatible with Mongo.
    const filters = strapi.utils.models.convertParams('transaksi', params);
    // Select field to populate.
    const populate = Transaksi.associations
      .filter(ast => ast.autoPopulate !== false)
      .map(ast => ast.alias)
      .join(' ');

    return Transaksi
      .find()
      .where(filters.where)
      .sort(filters.sort)
      .skip(filters.start)
      .limit(filters.limit)
      .populate(populate);
  },

  /**
   * Promise to fetch a/an transaksi.
   *
   * @return {Promise}
   */

  fetch: (params) => {
    // Select field to populate.
    const populate = Transaksi.associations
      .filter(ast => ast.autoPopulate !== false)
      .map(ast => ast.alias)
      .join(' ');

    return Transaksi
      .findOne(_.pick(params, _.keys(Transaksi.schema.paths)))
      .populate(populate);
  },

  /**
   * Promise to count transaksis.
   *
   * @return {Promise}
   */

  count: (params) => {
    // Convert `params` object to filters compatible with Mongo.
    const filters = strapi.utils.models.convertParams('transaksi', params);

    return Transaksi
      .count()
      .where(filters.where);
  },
  
  /**
   * Promise to add a/an transaksi.
   *
   * @return {Promise}
   */

  add: async (values) => {
    // Extract values related to relational data.
    const relations = _.pick(values, Transaksi.associations.map(ast => ast.alias));
    const data = _.omit(values, Transaksi.associations.map(ast => ast.alias));

    // Create entry with no-relational data.
    const entry = await Transaksi.create(data);

    // Create relational data and return the entry.
    return Transaksi.updateRelations({ _id: entry.id, values: relations });
  },

  /**
   * Promise to edit a/an transaksi.
   *
   * @return {Promise}
   */

  edit: async (params, values) => {
    // Extract values related to relational data.
    const relations = _.pick(values, Transaksi.associations.map(a => a.alias));
    const data = _.omit(values, Transaksi.associations.map(a => a.alias));

    // Update entry with no-relational data.
    const entry = await Transaksi.update(params, data, { multi: true });

    // Update relational data and return the entry.
    return Transaksi.updateRelations(Object.assign(params, { values: relations }));
  },

  /**
   * Promise to remove a/an transaksi.
   *
   * @return {Promise}
   */

  remove: async params => {
    // Select field to populate.
    const populate = Transaksi.associations
      .filter(ast => ast.autoPopulate !== false)
      .map(ast => ast.alias)
      .join(' ');

    // Note: To get the full response of Mongo, use the `remove()` method
    // or add spent the parameter `{ passRawResult: true }` as second argument.
    const data = await Transaksi
      .findOneAndRemove(params, {})
      .populate(populate);

    if (!data) {
      return data;
    }

    await Promise.all(
      Transaksi.associations.map(async association => {
        if (!association.via || !data._id) {
          return true;
        }

        const search = _.endsWith(association.nature, 'One') || association.nature === 'oneToMany' ? { [association.via]: data._id } : { [association.via]: { $in: [data._id] } };
        const update = _.endsWith(association.nature, 'One') || association.nature === 'oneToMany' ? { [association.via]: null } : { $pull: { [association.via]: data._id } };

        // Retrieve model.
        const model = association.plugin ?
          strapi.plugins[association.plugin].models[association.model || association.collection] :
          strapi.models[association.model || association.collection];

        return model.update(search, update, { multi: true });
      })
    );

    return data;
  },

  /**
   * Promise to search a/an transaksi.
   *
   * @return {Promise}
   */

  search: async (params) => {
    // Convert `params` object to filters compatible with Mongo.
    const filters = strapi.utils.models.convertParams('transaksi', params);
    // Select field to populate.
    const populate = Transaksi.associations
      .filter(ast => ast.autoPopulate !== false)
      .map(ast => ast.alias)
      .join(' ');

    const $or = Object.keys(Transaksi.attributes).reduce((acc, curr) => {
      switch (Transaksi.attributes[curr].type) {
        case 'integer':
        case 'float':
        case 'decimal':
          if (!_.isNaN(_.toNumber(params._q))) {
            return acc.concat({ [curr]: params._q });
          }

          return acc;
        case 'string':
        case 'text':
        case 'password':
          return acc.concat({ [curr]: { $regex: params._q, $options: 'i' } });
        case 'boolean':
          if (params._q === 'true' || params._q === 'false') {
            return acc.concat({ [curr]: params._q === 'true' });
          }

          return acc;
        default:
          return acc;
      }
    }, []);

    return Transaksi
      .find({ $or })
      .sort(filters.sort)
      .skip(filters.start)
      .limit(filters.limit)
      .populate(populate);
  },

  /**
   * Promise to inquiry a/an transaksi.
   *
   * @return {Promise}
   */

  inquiry: async (values) => {

    const finalResult= {};
    let wherePerusahaan = {};
    let whereTransaksi = {};
    let whereTransaksiDetail = {};
    let dataPeserta = [];

    finalResult['status'] = false;
    finalResult['message'] = '';
    finalResult['result'] = {};

    // validasi parameter post
    if(!values['kode_perusahaan'] || !values['periode']) {
      
      finalResult['status'] = false;
      finalResult['message'] = 'Invalid parameters.';
      finalResult['result'] = {};
      return finalResult;

    } else {

      // cari perusahaan by kode
      try {
        wherePerusahaan['kode_perusahaan'] = values['kode_perusahaan'];
        const dataPerusahaan = await Perusahaan
          .aggregate([ 
            { '$match': wherePerusahaan }
          ]);

        // jika perusahaan ditemukan
        if(dataPerusahaan.length > 0) {
          finalResult['result']['perusahaan'] = dataPerusahaan[0];
        }else {
          finalResult['result']['perusahaan'] = {};
        }
          

      } catch (error) {
        finalResult['status'] = false;
        finalResult['message'] = 'Something wrong in data perusahaan. ' + error;
        finalResult['result'] = {};
        return finalResult;
      }

      // cari data transaksi
      try {
        whereTransaksi['kode_perusahaan'] = values['kode_perusahaan'];
        whereTransaksi['periode'] = values['periode'];
        const dataTransaksi = await Transaksi
          .aggregate([ 
            { '$match': whereTransaksi },
            { '$project':
              {
                '_id': 1,
                'kode_perusahaan': 1,
                'periode': 1,
                'tanggal_bayar': 1,
                'jumlah_bayar': 1,
                'konfirm_status': 1,
                'konfirm_tanggal': 1,
              }
            },
          ]);

        // jika transaksi ditemukan
        if(dataTransaksi.length > 0) {

          finalResult['result']['transaksi'] = dataTransaksi[0];

          // cari peserta
          try {
            whereTransaksiDetail['transaksi'] = dataTransaksi[0]['_id'];
            console.log(whereTransaksiDetail);
            const dataTransaksiDetail = await Transaksidetail
              .aggregate([ 
                { '$match': whereTransaksiDetail },
                { '$lookup': 
                  { from: 'peserta', localField: 'peserta', foreignField: '_id', as: 'peserta' }
                },
                { '$unwind': '$peserta'},
                { '$lookup': 
                  { from: 'paket', localField: 'paket', foreignField: '_id', as: 'paket' }
                },
                { '$unwind': '$paket'},
                { '$project':
                  {
                    '_id': 1,
                    'tanggal_bayar': 1,
                    'jumlah_bayar': 1,
                    'konfirm_status': 1,
                    'konfirm_tanggal': 1,
                    'paket.kode_paket': 1,
                    'paket.nama_paket': 1,
                    'paket.jumlah_bayar': 1,
                    'peserta._id': 1,
                    'peserta.nama': 1,
                    'peserta.no_ktp': 1,
                    'peserta.no_bpjs': 1,
                    'peserta.jenis_kelamin': 1,
                    'peserta.tempat_lahir': 1,
                    'peserta.tanggal_lahir': 1,
                    'peserta.agama': 1,
                    'peserta.pekerjaan': 1,
                    'peserta.no_telp': 1,
                    'peserta.alamat': 1,
                    'peserta.status_aktif_pekerja': 1,
                  }
                },
              ]);
            
            // jika peserta ditemukan
            if(dataTransaksiDetail.length > 0){

              try {
                dataTransaksiDetail.forEach(element => {
                  element.peserta.paket = element.paket;
                  dataPeserta.push(element.peserta);
                });
                
                finalResult['result']['peserta'] = dataPeserta;
      
              } catch (error) {
                finalResult['status'] = false;
                finalResult['message'] = 'Something wrong in data peserta. ' + error;
                finalResult['result'] = {};
                return finalResult;  
              }
            }else {
              finalResult['result']['peserta'] = [];
            }
  
          } catch (error) {
            finalResult['status'] = false;
            finalResult['message'] = 'Something wrong in data detail transaksi. ' + error;
            finalResult['result'] = {};
            return finalResult;  
          }
        }
        else {
          finalResult['result']['transaksi'] = {};
          finalResult['result']['peserta'] = [];
        }
          
      } catch (error) {
        finalResult['status'] = false;
        finalResult['message'] = 'Something wrong in data transaksi. ' + error;
        finalResult['result'] = {};
        return finalResult;  
      }

      finalResult['status'] = true;
      finalResult['message'] = 'Ok.'; 
      return finalResult;
    }
    
    
  },

  
  /**
   * Promise to confirm a/an transaksi.
   *
   * @return {Promise}
   */

  confirm: async (values) => {
    // Extract values related to relational data.
    const finalResult= {}; 
    let whereData = {}; 
    let updateData = {}; 
    let dataTransaksi = {}; 

    finalResult['status'] = false;
    finalResult['message'] = '';
    finalResult['result'] = {};

    if(!values['kode_perusahaan'] || !values['periode'] || !values['jumlah_bayar'] || !values['status'].toString()) {

      finalResult['status'] = false;
      finalResult['message'] = 'Invalid parameters.';
      finalResult['result'] = {};
      return finalResult;

    }else {

      try {
        whereData['kode_perusahaan'] = values['kode_perusahaan'];
        whereData['periode'] = values['periode'];
        whereData['jumlah_bayar'] = Number(values['jumlah_bayar']);
        if(values['status'].toString() == 'true'){
          whereData['konfirm_status'] = true;
        }else{
          whereData['konfirm_status'] = false;
        }

        updateData['konfirm_status'] = !whereData['konfirm_status'];
        updateData['konfirm_tanggal'] = moment().toISOString();
        

        const dataTransaksi = await Transaksi
          .aggregate([ 
            { '$match': whereData },
            { '$project':
              {
                '_id': 1,
                'kode_perusahaan': 1,
                'periode': 1,
                'tanggal_bayar': 1,
                'jumlah_bayar': 1,
                'konfirm_status': 1,
                'konfirm_tanggal': 1,
              }
            },
          ]);

        if(dataTransaksi.length > 0) {
          
          dataTransaksi[0]['konfirm_status'] = !whereData['konfirm_status'];
          dataTransaksi[0]['konfirm_tanggal'] =  moment().toISOString();
          finalResult['result'] = dataTransaksi[0];

          try { 
            const update_transaksi = await Transaksi.update(whereData, updateData, { multi: false });

          } catch (error) { 
            finalResult['status'] = false;
            finalResult['message'] = 'Something wrong on update status transaksi.';
            finalResult['result'] = {};
          }
 
          try {
            let whereData2 = {}; 
            let updateData2 = {}; 

            whereData2['transaksi'] = dataTransaksi[0]['_id'];  
            updateData2['konfirm_status'] = !whereData['konfirm_status'];
            updateData2['konfirm_tanggal'] = moment().format('DD MMMM YYYY hh:mm:ss');
            const update_transaksi_detail = await Transaksidetail.update(whereData2, updateData2, { multi: true });

          } catch (error) { 
            finalResult['status'] = false;
            finalResult['message'] = 'Something wrong on update status detail transaksi.';
            finalResult['result'] = {};
          }

        }else {
          finalResult['status'] = false;
          finalResult['message'] = 'Data transaction not match. ';
          finalResult['result'] = {};
          return finalResult;  
        }
          
      } catch (error) {
        finalResult['status'] = false;
        finalResult['message'] = 'Something wrong in data transaksi. ' + error;
        finalResult['result'] = {};
        return finalResult;  
      }

    }
   
    finalResult['status'] = true;
    finalResult['message'] = 'Transaction confirmation success.';
    return finalResult;
  }
};
