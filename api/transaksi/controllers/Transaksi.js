'use strict';

/**
 * Transaksi.js controller
 *
 * @description: A set of functions called "actions" for managing `Transaksi`.
 */

module.exports = {

  /**
   * Retrieve transaksi records.
   *
   * @return {Object|Array}
   */

  find: async (ctx) => {
    if (ctx.query._q) {
      return strapi.services.transaksi.search(ctx.query);
    } else {
      return strapi.services.transaksi.fetchAll(ctx.query);
    }
  },

  /**
   * Retrieve a transaksi record.
   *
   * @return {Object}
   */

  findOne: async (ctx) => {
    if (!ctx.params._id.match(/^[0-9a-fA-F]{24}$/)) {
      return ctx.notFound();
    }

    return strapi.services.transaksi.fetch(ctx.params);
  },

  /**
   * Count transaksi records.
   *
   * @return {Number}
   */

  count: async (ctx) => {
    return strapi.services.transaksi.count(ctx.query);
  },

  /**
   * Create a/an transaksi record.
   *
   * @return {Object}
   */

  create: async (ctx) => {
    return strapi.services.transaksi.add(ctx.request.body);
  },

  /**
   * Update a/an transaksi record.
   *
   * @return {Object}
   */

  update: async (ctx, next) => {
    return strapi.services.transaksi.edit(ctx.params, ctx.request.body) ;
  },

  /**
   * Destroy a/an transaksi record.
   *
   * @return {Object}
   */

  destroy: async (ctx, next) => {
    return strapi.services.transaksi.remove(ctx.params);
  },

  /**
   * inquiry a/an transaksi record.
   *
   * @return {Object}
   */

  inquiry: async (ctx) => {
    return strapi.services.transaksi.inquiry(ctx.request.body);
  },
  
  /**
  confirm: async (ctx) => {
   * Create a/an transaksi record.
   *
   * @return {Object}
   */

  confirm: async (ctx) => {
    return strapi.services.transaksi.confirm(ctx.request.body);
  }
};
