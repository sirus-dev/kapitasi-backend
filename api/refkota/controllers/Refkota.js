'use strict';

/**
 * Refkota.js controller
 *
 * @description: A set of functions called "actions" for managing `Refkota`.
 */

module.exports = {

  /**
   * Retrieve refkota records.
   *
   * @return {Object|Array}
   */

  find: async (ctx) => {
    if (ctx.query._q) {
      return strapi.services.refkota.search(ctx.query);
    } else {
      return strapi.services.refkota.fetchAll(ctx.query);
    }
  },

  /**
   * Retrieve a refkota record.
   *
   * @return {Object}
   */

  findOne: async (ctx) => {
    if (!ctx.params._id.match(/^[0-9a-fA-F]{24}$/)) {
      return ctx.notFound();
    }

    return strapi.services.refkota.fetch(ctx.params);
  },

  /**
   * Count refkota records.
   *
   * @return {Number}
   */

  count: async (ctx) => {
    return strapi.services.refkota.count(ctx.query);
  },

  /**
   * Create a/an refkota record.
   *
   * @return {Object}
   */

  create: async (ctx) => {
    return strapi.services.refkota.add(ctx.request.body);
  },

  /**
   * Update a/an refkota record.
   *
   * @return {Object}
   */

  update: async (ctx, next) => {
    return strapi.services.refkota.edit(ctx.params, ctx.request.body) ;
  },

  /**
   * Destroy a/an refkota record.
   *
   * @return {Object}
   */

  destroy: async (ctx, next) => {
    return strapi.services.refkota.remove(ctx.params);
  }
};
