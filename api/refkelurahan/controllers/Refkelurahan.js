'use strict';

/**
 * Refkelurahan.js controller
 *
 * @description: A set of functions called "actions" for managing `Refkelurahan`.
 */

module.exports = {

  /**
   * Retrieve refkelurahan records.
   *
   * @return {Object|Array}
   */

  find: async (ctx) => {
    if (ctx.query._q) {
      return strapi.services.refkelurahan.search(ctx.query);
    } else {
      return strapi.services.refkelurahan.fetchAll(ctx.query);
    }
  },

  /**
   * Retrieve a refkelurahan record.
   *
   * @return {Object}
   */

  findOne: async (ctx) => {
    if (!ctx.params._id.match(/^[0-9a-fA-F]{24}$/)) {
      return ctx.notFound();
    }

    return strapi.services.refkelurahan.fetch(ctx.params);
  },

  /**
   * Count refkelurahan records.
   *
   * @return {Number}
   */

  count: async (ctx) => {
    return strapi.services.refkelurahan.count(ctx.query);
  },

  /**
   * Create a/an refkelurahan record.
   *
   * @return {Object}
   */

  create: async (ctx) => {
    return strapi.services.refkelurahan.add(ctx.request.body);
  },

  /**
   * Update a/an refkelurahan record.
   *
   * @return {Object}
   */

  update: async (ctx, next) => {
    return strapi.services.refkelurahan.edit(ctx.params, ctx.request.body) ;
  },

  /**
   * Destroy a/an refkelurahan record.
   *
   * @return {Object}
   */

  destroy: async (ctx, next) => {
    return strapi.services.refkelurahan.remove(ctx.params);
  }
};
