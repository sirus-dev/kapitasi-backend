'use strict';

/**
 * Transaksidetail.js controller
 *
 * @description: A set of functions called "actions" for managing `Transaksidetail`.
 */

module.exports = {

  /**
   * Retrieve transaksidetail records.
   *
   * @return {Object|Array}
   */

  find: async (ctx) => {
    if (ctx.query._q) {
      return strapi.services.transaksidetail.search(ctx.query);
    } else {
      return strapi.services.transaksidetail.fetchAll(ctx.query);
    }
  },

  /**
   * Retrieve a transaksidetail record.
   *
   * @return {Object}
   */

  findOne: async (ctx) => {
    if (!ctx.params._id.match(/^[0-9a-fA-F]{24}$/)) {
      return ctx.notFound();
    }

    return strapi.services.transaksidetail.fetch(ctx.params);
  },

  /**
   * Count transaksidetail records.
   *
   * @return {Number}
   */

  count: async (ctx) => {
    return strapi.services.transaksidetail.count(ctx.query);
  },

  /**
   * Create a/an transaksidetail record.
   *
   * @return {Object}
   */

  create: async (ctx) => {
    return strapi.services.transaksidetail.add(ctx.request.body);
  },

  /**
   * Update a/an transaksidetail record.
   *
   * @return {Object}
   */

  update: async (ctx, next) => {
    return strapi.services.transaksidetail.edit(ctx.params, ctx.request.body) ;
  },

  /**
   * Destroy a/an transaksidetail record.
   *
   * @return {Object}
   */

  destroy: async (ctx, next) => {
    return strapi.services.transaksidetail.remove(ctx.params);
  }
};
