'use strict';

/**
 * Rumahsakit.js controller
 *
 * @description: A set of functions called "actions" for managing `Rumahsakit`.
 */

module.exports = {

  /**
   * Retrieve rumahsakit records.
   *
   * @return {Object|Array}
   */

  find: async (ctx) => {
    if (ctx.query._q) {
      return strapi.services.rumahsakit.search(ctx.query);
    } else {
      return strapi.services.rumahsakit.fetchAll(ctx.query);
    }
  },

  /**
   * Retrieve a rumahsakit record.
   *
   * @return {Object}
   */

  findOne: async (ctx) => {
    if (!ctx.params._id.match(/^[0-9a-fA-F]{24}$/)) {
      return ctx.notFound();
    }

    return strapi.services.rumahsakit.fetch(ctx.params);
  },

  /**
   * Count rumahsakit records.
   *
   * @return {Number}
   */

  count: async (ctx) => {
    return strapi.services.rumahsakit.count(ctx.query);
  },

  /**
   * Create a/an rumahsakit record.
   *
   * @return {Object}
   */

  create: async (ctx) => {
    return strapi.services.rumahsakit.add(ctx.request.body);
  },

  /**
   * Update a/an rumahsakit record.
   *
   * @return {Object}
   */

  update: async (ctx, next) => {
    return strapi.services.rumahsakit.edit(ctx.params, ctx.request.body) ;
  },

  /**
   * Destroy a/an rumahsakit record.
   *
   * @return {Object}
   */

  destroy: async (ctx, next) => {
    return strapi.services.rumahsakit.remove(ctx.params);
  }
};
