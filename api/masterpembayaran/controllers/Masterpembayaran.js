'use strict';

/**
 * Masterpembayaran.js controller
 *
 * @description: A set of functions called "actions" for managing `Masterpembayaran`.
 */

module.exports = {

  /**
   * Retrieve masterpembayaran records.
   *
   * @return {Object|Array}
   */

  find: async (ctx) => {
    if (ctx.query._q) {
      return strapi.services.masterpembayaran.search(ctx.query);
    } else {
      return strapi.services.masterpembayaran.fetchAll(ctx.query);
    }
  },

  /**
   * Retrieve a masterpembayaran record.
   *
   * @return {Object}
   */

  findOne: async (ctx) => {
    if (!ctx.params._id.match(/^[0-9a-fA-F]{24}$/)) {
      return ctx.notFound();
    }

    return strapi.services.masterpembayaran.fetch(ctx.params);
  },

  /**
   * Count masterpembayaran records.
   *
   * @return {Number}
   */

  count: async (ctx) => {
    return strapi.services.masterpembayaran.count(ctx.query);
  },

  /**
   * Create a/an masterpembayaran record.
   *
   * @return {Object}
   */

  create: async (ctx) => {
    return strapi.services.masterpembayaran.add(ctx.request.body);
  },

  /**
   * Update a/an masterpembayaran record.
   *
   * @return {Object}
   */

  update: async (ctx, next) => {
    return strapi.services.masterpembayaran.edit(ctx.params, ctx.request.body) ;
  },

  /**
   * Destroy a/an masterpembayaran record.
   *
   * @return {Object}
   */

  destroy: async (ctx, next) => {
    return strapi.services.masterpembayaran.remove(ctx.params);
  }
};
