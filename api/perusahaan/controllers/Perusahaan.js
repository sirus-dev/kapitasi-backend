'use strict';

/**
 * Perusahaan.js controller
 *
 * @description: A set of functions called "actions" for managing `Perusahaan`.
 */

module.exports = {

  /**
   * Retrieve perusahaan records.
   *
   * @return {Object|Array}
   */

  find: async (ctx) => {
    if (ctx.query._q) {
      return strapi.services.perusahaan.search(ctx.query);
    } else {
      return strapi.services.perusahaan.fetchAll(ctx.query);
    }
  },

  /**
   * Retrieve a perusahaan record.
   *
   * @return {Object}
   */

  findOne: async (ctx) => {
    if (!ctx.params._id.match(/^[0-9a-fA-F]{24}$/)) {
      return ctx.notFound();
    }

    return strapi.services.perusahaan.fetch(ctx.params);
  },

  /**
   * Count perusahaan records.
   *
   * @return {Number}
   */

  count: async (ctx) => {
    return strapi.services.perusahaan.count(ctx.query);
  },

  /**
   * Create a/an perusahaan record.
   *
   * @return {Object}
   */

  create: async (ctx) => {
    return strapi.services.perusahaan.add(ctx.request.body);
  },

  /**
   * Update a/an perusahaan record.
   *
   * @return {Object}
   */

  update: async (ctx, next) => {
    return strapi.services.perusahaan.edit(ctx.params, ctx.request.body) ;
  },

  /**
   * Destroy a/an perusahaan record.
   *
   * @return {Object}
   */

  destroy: async (ctx, next) => {
    return strapi.services.perusahaan.remove(ctx.params);
  }
};
