'use strict';

/**
 * Jurnal.js service
 *
 * @description: A set of functions similar to controller's actions to avoid code duplication.
 */

// Public dependencies.
const _ = require('lodash');

module.exports = {

  /**
   * Promise to fetch all jurnals.
   *
   * @return {Promise}
   */

  fetchAll: (params) => {
    // Convert `params` object to filters compatible with Mongo.
    const filters = strapi.utils.models.convertParams('jurnal', params);
    // Select field to populate.
    const populate = Jurnal.associations
      .filter(ast => ast.autoPopulate !== false)
      .map(ast => ast.alias)
      .join(' ');

    return Jurnal
      .find()
      .where(filters.where)
      .sort(filters.sort)
      .skip(filters.start)
      .limit(filters.limit)
      .populate(populate);
  },

  /**
   * Promise to fetch a/an jurnal.
   *
   * @return {Promise}
   */

  fetch: (params) => {
    // Select field to populate.
    const populate = Jurnal.associations
      .filter(ast => ast.autoPopulate !== false)
      .map(ast => ast.alias)
      .join(' ');

    return Jurnal
      .findOne(_.pick(params, _.keys(Jurnal.schema.paths)))
      .populate(populate);
  },

  /**
   * Promise to count jurnals.
   *
   * @return {Promise}
   */

  count: (params) => {
    // Convert `params` object to filters compatible with Mongo.
    const filters = strapi.utils.models.convertParams('jurnal', params);

    return Jurnal
      .count()
      .where(filters.where);
  },

  /**
   * Promise to add a/an jurnal.
   *
   * @return {Promise}
   */

  add: async (values) => {
    // Extract values related to relational data.
    const relations = _.pick(values, Jurnal.associations.map(ast => ast.alias));
    const data = _.omit(values, Jurnal.associations.map(ast => ast.alias));

    // Create entry with no-relational data.
    const entry = await Jurnal.create(data);

    // Create relational data and return the entry.
    return Jurnal.updateRelations({ _id: entry.id, values: relations });
  },

  /**
   * Promise to edit a/an jurnal.
   *
   * @return {Promise}
   */

  edit: async (params, values) => {
    // Extract values related to relational data.
    const relations = _.pick(values, Jurnal.associations.map(a => a.alias));
    const data = _.omit(values, Jurnal.associations.map(a => a.alias));

    // Update entry with no-relational data.
    const entry = await Jurnal.update(params, data, { multi: true });

    // Update relational data and return the entry.
    return Jurnal.updateRelations(Object.assign(params, { values: relations }));
  },

  /**
   * Promise to remove a/an jurnal.
   *
   * @return {Promise}
   */

  remove: async params => {
    // Select field to populate.
    const populate = Jurnal.associations
      .filter(ast => ast.autoPopulate !== false)
      .map(ast => ast.alias)
      .join(' ');

    // Note: To get the full response of Mongo, use the `remove()` method
    // or add spent the parameter `{ passRawResult: true }` as second argument.
    const data = await Jurnal
      .findOneAndRemove(params, {})
      .populate(populate);

    if (!data) {
      return data;
    }

    await Promise.all(
      Jurnal.associations.map(async association => {
        if (!association.via || !data._id) {
          return true;
        }

        const search = _.endsWith(association.nature, 'One') || association.nature === 'oneToMany' ? { [association.via]: data._id } : { [association.via]: { $in: [data._id] } };
        const update = _.endsWith(association.nature, 'One') || association.nature === 'oneToMany' ? { [association.via]: null } : { $pull: { [association.via]: data._id } };

        // Retrieve model.
        const model = association.plugin ?
          strapi.plugins[association.plugin].models[association.model || association.collection] :
          strapi.models[association.model || association.collection];

        return model.update(search, update, { multi: true });
      })
    );

    return data;
  },

  /**
   * Promise to search a/an jurnal.
   *
   * @return {Promise}
   */

  search: async (params) => {
    // Convert `params` object to filters compatible with Mongo.
    const filters = strapi.utils.models.convertParams('jurnal', params);
    // Select field to populate.
    const populate = Jurnal.associations
      .filter(ast => ast.autoPopulate !== false)
      .map(ast => ast.alias)
      .join(' ');

    const $or = Object.keys(Jurnal.attributes).reduce((acc, curr) => {
      switch (Jurnal.attributes[curr].type) {
        case 'integer':
        case 'float':
        case 'decimal':
          if (!_.isNaN(_.toNumber(params._q))) {
            return acc.concat({ [curr]: params._q });
          }

          return acc;
        case 'string':
        case 'text':
        case 'password':
          return acc.concat({ [curr]: { $regex: params._q, $options: 'i' } });
        case 'boolean':
          if (params._q === 'true' || params._q === 'false') {
            return acc.concat({ [curr]: params._q === 'true' });
          }

          return acc;
        default:
          return acc;
      }
    }, []);

    return Jurnal
      .find({ $or })
      .sort(filters.sort)
      .skip(filters.start)
      .limit(filters.limit)
      .populate(populate);
  },


  /**
   * Promise to inquiry a/an transaksi.
   *
   * @return {Promise}
   */

  countPembayaran: async (values) => {

    
    let whereMasterPembayaran = {};
    let whereTransaksi = {};
    const finalResult= {};

    whereMasterPembayaran['aktif_status'] = true;
    const dataMasterPembayaran = await Masterpembayaran
      .aggregate([
        { '$match': whereMasterPembayaran }
      ]);

    whereTransaksi['konfirm_status'] = true;
    const dataTransaksi = await Transaksi
      .aggregate([
        { '$match': whereTransaksi },
        { '$lookup': 
          { from: 'transaksidetail', localField: '_id', foreignField: 'transaksi', as: 'transaksidetail' }
        }
      ]);

    // jika transaksi ditemukan
    if(dataTransaksi.length > 0) {

      dataTransaksi.forEach(element => {
        element['pembayaran'] = {};
        element['persentase'] = {};
        element['jumlah_peserta'] = element['transaksidetail'].length;
        dataMasterPembayaran.forEach(el => {
          element['pembayaran'][el['nama'].replace(' ', '-')] = (Number(element['jumlah_bayar']) * Number(el['persentase'])) / 100;
          element['persentase'][el['nama'].replace(' ', '-')] = Number(el['persentase']);
        });
      });

      finalResult['result'] = dataTransaksi;

    }else {
      finalResult['result'] = [];
    }

    
    finalResult['status'] = true;
    finalResult['message'] = 'Ok.'; 
    return finalResult;

  }

};
