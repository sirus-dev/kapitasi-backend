'use strict';

/**
 * Paket.js controller
 *
 * @description: A set of functions called "actions" for managing `Paket`.
 */

module.exports = {

  /**
   * Retrieve paket records.
   *
   * @return {Object|Array}
   */

  find: async (ctx) => {
    if (ctx.query._q) {
      return strapi.services.paket.search(ctx.query);
    } else {
      return strapi.services.paket.fetchAll(ctx.query);
    }
  },

  /**
   * Retrieve a paket record.
   *
   * @return {Object}
   */

  findOne: async (ctx) => {
    if (!ctx.params._id.match(/^[0-9a-fA-F]{24}$/)) {
      return ctx.notFound();
    }

    return strapi.services.paket.fetch(ctx.params);
  },

  /**
   * Count paket records.
   *
   * @return {Number}
   */

  count: async (ctx) => {
    return strapi.services.paket.count(ctx.query);
  },

  /**
   * Create a/an paket record.
   *
   * @return {Object}
   */

  create: async (ctx) => {
    return strapi.services.paket.add(ctx.request.body);
  },

  /**
   * Update a/an paket record.
   *
   * @return {Object}
   */

  update: async (ctx, next) => {
    return strapi.services.paket.edit(ctx.params, ctx.request.body) ;
  },

  /**
   * Destroy a/an paket record.
   *
   * @return {Object}
   */

  destroy: async (ctx, next) => {
    return strapi.services.paket.remove(ctx.params);
  }
};
